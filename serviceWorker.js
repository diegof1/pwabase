const assets = [
    "index.html",
    "css/estilo.css",
    "js/app.js"
    
]
const statDevPWA = "br.edu.cest.boca-dev-v1"

self.addEventListener("install", (installEvent)  => {
    installEvent.waitUntil(
        caches.open(statDevPWA).then(
            (cache) => cache.addAll(assets)));
 }
);

self.addEventListener("fetch", (fetchEvent)  => {
    console.log('Fetch url capturada:', fetchEvent.request.url);
    fetchEvent.respondWith(
        caches.match(fetchEvent.request).then(
            (cachedResponse) => {
                if (cachedResponse) {
                    return cachedResponse;
                }
                return fetch(fetchEvent.request);

        }),
    );
});
